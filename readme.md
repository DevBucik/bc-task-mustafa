Create simple "Private notes" web app in Java Spring Boot & AngularJS

* if you encounter difficulties with the task (at any phase of implementation) - write to us describing the problem, we will send you a hint

* feel free to use Google & StackOverflow, especially for angularjs, html, css syntax

1. Create UI as shown in mockup.png (HTML, CSS)

	* use Angular Material components:
	https://material.angularjs.org/latest/demo/input

2. In Java, create "/add-note" POST request handler. It should accept param in JSON format with "name" and "content" fields. These fields should contains data entered previously by user (in input elements shown in mockup.png)

3. Button "ADD NOTE" should call our POST endpoint (/add-note) by passing necessary data (name & content from input elements). Server should read this data and save note as txt file (name is filename, and content is file content). Server should return:
	- if name is "very important private note" than server should return information about saving this very important private note. On the front-end side - user should be informed about this fact ("very important note was saved")
	- if name is "public note" than server should return an error and user should be prompted about this fact
	- It shouldn't be possible to save empty note (with empty name or empty content)
	- Otherwise - front-end should display message about successful save

4. After handling success response from server you should add new row in table shown in mockup.png (with entered name & content)

* this task only requires the note to be saved in file, you do not have to implement operation of fetching and displaying the list of notes when the page is loaded - you can also skip deleting and editing operations

----------------------------------------------------------------

After you finished:

Please commit changes to this repo