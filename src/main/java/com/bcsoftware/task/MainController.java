package com.bcsoftware.task;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class MainController {
	@RequestMapping(path="/task", method = RequestMethod.GET) 
	public String menuAdminView(Map<String, Object> model) {

		return "task/task";
    }    
}
