
 
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Simple task in AngularJS by BC SOFTWARE</title>

    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.9/angular.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.9/angular-animate.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.9/angular-aria.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.9/angular-messages.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.9/angular-sanitize.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angular_material/1.1.7/angular-material.js"></script>
    <link rel="stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/angular_material/1.1.7/angular-material.min.css">

	<script src="${contextPath}/resources/angular/components/script.js"></script>
    <link rel="stylesheet" type="text/css" href="${contextPath}/resources/angular/components/styles.css">

</head>
<body ng-app="ngApp">
    

    <div id="content" ng-controller="ngMainController">

        <h1>Private notes</h1>

        <!-- code here -->

        <md-button class="md-raised md-primary">ADD NOTE</md-button>

    </div>

</body>
</html>